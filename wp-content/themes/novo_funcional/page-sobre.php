<?php get_header(); ?>
<img src=" <?php header_image(); ?>" height="<?php echo get_custom_header()->height;?>" widht="<?php echo get_custom_header()->width;?>">

<body>
    <div class="container">
        <div class="sobre">
            <img src="" alt="">
        </div>
        <div class="contsob flex">
           <div class="col-4"><h2><?php _e ('About Us', 'wpcurso')?></h2>
            <p>Once upon a midnight dreary, while I pondered, weak and weary,<br>
            Over many a quaint and curious volume of forgotten lore—<br>
                While I nodded, nearly napping, suddenly there came a tapping,<br>
            As of some one gently rapping, rapping at my chamber door.<br>
            “’Tis some visitor,” I muttered, “tapping at my chamber door—<br>
                        Only this and nothing more.”</p>
                        <p>  Ah, distinctly I remember it was in the bleak December;<br>
            And each separate dying ember wrought its ghost upon the floor.<br>
                Eagerly I wished the morrow;—vainly I had sought to borrow<br>
                From my books surcease of sorrow—sorrow for the lost Lenore—<br>
            For the rare and radiant maiden whom the angels name Lenore—<br>
                        Nameless here for evermore.<br></p>
                      
           </div>

                <div class="imgsobre col-4">
                <img src="<?php echo get_template_directory_uri(); ?>/images/crow.jpg">
                  <h3><?php _e ('The Crows','wpcurso')?></h3> 
                </div>

             <div class="sobrebarralateral col-4">
                
                    <?php get_sidebar('sobre');?>

              </div>
        </div>
        <div class="extratext flex">
          <p>And the silken, sad, uncertain rustling of each purple curtain<br>
            Thrilled me—filled me with fantastic terrors never felt before;<br>
                So that now, to still the beating of my heart, I stood repeating<br>
                “’Tis some visitor entreating entrance at my chamber door—<br>
            Some late visitor entreating entrance at my chamber door;—<br>
                        This it is and nothing more.”<br></p>
                        <p>
                        Presently my soul grew stronger; hesitating then no longer,<br>
            “Sir,” said I, “or Madam, truly your forgiveness I implore;<br>
                But the fact is I was napping, and so gently you came rapping,<br>
                And so faintly you came tapping, tapping at my chamber door,<br>
            That I scarce was sure I heard you”—here I opened wide the door;—<br>
                        Darkness there and nothing more.
                        </p>
                        <p>And the silken, sad, uncertain rustling of each purple curtain<br>
            Thrilled me—filled me with fantastic terrors never felt before;<br>
                So that now, to still the beating of my heart, I stood repeating<br>
                “’Tis some visitor entreating entrance at my chamber door—<br>
            Some late visitor entreating entrance at my chamber door;—<br>
                        This it is and nothing more.”<br></p>
        </div>
   </div>
   <div>
       <?php
       $key = get_theme_mod('set_map_apikey');
       $address = urlencode(get_theme_mod('set_map_address'));
       ?>
   <iframe
   magin-top="30px"
  width="100%"
  height="450"
  style="border:0"
  loading="lazy"
  allowfullscreen
  referrerpolicy="no-referrer-when-downgrade"
  src="https://www.google.com/maps/embed/v1/place?key=<?php echo $key;?>
    &q=<?php echo $address; ?>&zoom=15">
</iframe>
   </div>
</body>
<?php get_footer(); ?>