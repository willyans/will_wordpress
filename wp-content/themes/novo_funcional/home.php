<?php get_header(); ?>
<img src=" <?php header_image(); ?>" height="<?php echo get_custom_header()->height;?>" widht="<?php echo get_custom_header()->width;?>">

<body>
    <div class="container flex">
        <div class="imgprincipal col-6">
            
            
        </div>
        <div class="textoprincipal col-6 text-left">
            <h3><?php _e('Beautiful Title','wpcurso')?></h3>
        <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit.<br> 
                Suscipit a vel, ipsum fugiat aperiam eveniet odit unde consectetur<br> 
                voluptatem pariatur laudantium totam blanditiis minus!<br> 
                Repellat aliquid sunt sed molestias odio!</p>
                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit.<br> 
                Suscipit a vel, ipsum fugiat aperiam eveniet odit unde consectetur<br> 
                voluptatem pariatur laudantium totam blanditiis minus!<br> 
                Repellat aliquid sunt sed molestias odio!</p>
                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit.<br> 
                Suscipit a vel, ipsum fugiat aperiam eveniet odit unde consectetur<br> 
                voluptatem pariatur laudantium totam blanditiis minus!<br> 
                Repellat aliquid sunt sed molestias odio!</p>
                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit.<br> 
                Suscipit a vel, ipsum fugiat aperiam eveniet odit unde consectetur<br> 
                voluptatem pariatur laudantium totam blanditiis minus!<br> 
                Repellat aliquid sunt sed molestias odio!</p>
            
        </div>
       
    </div>
    Photo by <a href="https://unsplash.com/@nicnut?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Nicola Nuttall</a> on <a href="https://unsplash.com/s/photos/classic?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>
    <hr>
    <div class="container">
        <div class="row">
            <?php
                //primeiro post
                 $featured = new WP_Query('post_type=post&posts_per_page=1&cat=15');

                 if($featured->have_posts()):
                     while($featured->have_posts()): $featured->the_post();
                ?>
                  <div class="col-12">
                     <?php get_template_part( 'template-parts/content', 'featured' );?>
                  </div>
                <?php
                    endwhile;
                 wp_reset_postdata();
                endif;
               //segundo post
               $args = array(
                   'post_type' => 'post',
                   'posts_per_page' => 2,
                   'category__not_in' => array(),
                   'category__in' => array(14,24)

               );
                $secundary = new WP_Query( $args );

                if($secundary->have_posts()):
                    while($secundary->have_posts()): $secundary->the_post();
               ?>
                 <div class="col-6">
                    <?php get_template_part( 'template-parts/content', 'secundary' );?>
                 </div>
               <?php
                   endwhile;
                wp_reset_postdata();
               endif;
             ?>

             <?php
                //terceiro post
                 $featured = new WP_Query('post_type=post&posts_per_page=1&cat=10');

                 if($featured->have_posts()):
                     while($featured->have_posts()): $featured->the_post();
                ?>
                  <div class="col-12">
                     <?php get_template_part( 'template-parts/content', 'half' );?>
                  </div>
                <?php
                    endwhile;
                 wp_reset_postdata();
                endif;
                ?>
        </div>

        <?php
                //quarto post
                 $featured = new WP_Query('post_type=post&posts_per_page=1&cat=29');

                 if($featured->have_posts()):
                     while($featured->have_posts()): $featured->the_post();
                ?>
                  <div class="col-12">
                     <?php get_template_part( 'template-parts/content', 'half' );?>
                  </div>
                <?php
                    endwhile;
                 wp_reset_postdata();
                endif;
                ?>
        </div>
        <?php
                //quinto post
                 $featured = new WP_Query('post_type=post&posts_per_page=1&cat=30');

                 if($featured->have_posts()):
                     while($featured->have_posts()): $featured->the_post();
                ?>
                  <div class="col-12">
                     <?php get_template_part( 'template-parts/content', 'half' );?>
                  </div>
                <?php
                    endwhile;
                 wp_reset_postdata();
                endif;
                ?>
        </div>
    </div>

</body>

<?php get_footer(); ?>

