<?php get_header()?>
<img src=" <?php header_image(); ?>" height="<?php echo get_custom_header()->height;?>" widht="<?php echo get_custom_header()->width;?>">

<div class="container flex">
  <div class="col-6">
      <h2>search results for: </h2><?php echo get_search_query();?>

       <?php 

       get_search_form();
       
       while(have_posts()): the_post();

       get_template_part( 'template-parts/content', 'search' );
       
          if(comments_open() || get_comments_number() ) 
            comments_template();
        

        endwhile;
        
       
        
      the_posts_pagination(
         array( 
             'prev_text' => 'Previous',
             'next_text' =>  'Next'
         )
      );
    ?>
   
</div>
    </div>


<?php get_footer()?>