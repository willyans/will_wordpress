<?php

// incluindo arquivos TGM
require_once get_template_directory() . '/inc/class-tgm-plugin-activation.php';
require_once get_template_directory() . '/inc/required-plugins.php';

// requerendo o arquivo customizer
require get_template_directory() . '/inc/customizer.php';

//carregando scripts e paginas de estilo

function load_scripts(){
    wp_enqueue_script('bootstrap-js', 'https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js', array('jquery'),('5.1.0'), true);

    
    wp_enqueue_style( 'bootstrap-css' , 'https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css', 
    array(), '5.1.3', 'all' );

    wp_enqueue_style( 'template' , get_template_directory_uri() . '/css/template.css', 
    array(), '1.0', 'all' );
}

add_action('wp_enqueue_scripts', 'load_scripts' );


/* função de configuração de thema*/
function wpcurso_config() {
    //registrando menus
register_nav_menus(
    array(
        'my_main_menu' => 'Main Menu',
        'footer_menu' => 'Footer Menu'
    )
);
   $args = array(
       'height' => 225,
       'width' => 1920
   );
   add_theme_support('custom-header', $args);
   add_theme_support('post-thumbnails');
   add_theme_support('post-formats', array( 'video', 'image', 'status', 'chat'));
   add_theme_support('title-tag');
   add_theme_support('custom-logo', array( 'height' => 110, 'width' => 200));

// Habilitando suporte a tradução
$textdomain = 'wpcurso';

load_theme_textdomain($textdomain, get_template_directory() . '/languages/');
}

add_action( 'after_setup_theme', 'wpcurso_config', 0 );

add_action( 'widgets_init','wpcurso_sidebars');
function wpcurso_sidebars(){
    register_sidebar( array( 'name' => 'Side Bar teste',
    'id' => 'sidebar-1',
    'description' => 'sidebar for tests',
    'before_width' => '<div class="widget-wrapper">',
    'after_width' => '</div>',
    'before_title' => '<h2 class="width-title">',
    'afeter_title' => '</h2>',
)
);
register_sidebar( array( 'name' => 'Side Bar contact',
    'id' => 'sidebar-2',
    'description' => 'sidebar for tests contact',
    'before_width' => '<div class="widget-wrapper">',
    'after_width' => '</div>',
    'before_title' => '<h2 class="width-title">',
    'afeter_title' => '</h2>',
)
);
register_sidebar( array( 'name' => 'service1',
    'id' => 'service-1',
    'description' => 'service for tests',
    'before_width' => '<div class="widget-wrapper">',
    'after_width' => '</div>',
    'before_title' => '<h2 class="width-title">',
    'afeter_title' => '</h2>',
)
);
register_sidebar( array( 'name' => 'service2',
    'id' => 'service-2',
    'description' => 'service for tests',
    'before_width' => '<div class="widget-wrapper">',
    'after_width' => '</div>',
    'before_title' => '<h2 class="width-title">',
    'afeter_title' => '</h2>',
)
);
register_sidebar( array( 'name' => 'Service3',
    'id' => 'service-3',
    'description' => 'service for tests',
    'before_width' => '<div class="widget-wrapper">',
    'after_width' => '</div>',
    'before_title' => '<h2 class="width-title">',
    'afeter_title' => '</h2>',
)
);
};