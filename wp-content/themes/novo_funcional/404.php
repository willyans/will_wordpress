<?php get_header(); ?>

 <div class="container">   

<h1><?php _e( 'Page Not Found...','wpcurso')?></h1>
<img class="img-fluid" style=""src="<?php echo get_template_directory_uri(); ?>/images/cat_404.jpg">

Photo by <a href="https://unsplash.com/@snowboardinec?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Max Baskakov</a> on <a href="https://unsplash.com/?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>
</div>
<?php get_footer(); ?>