<?php get_header(); ?>


<div class="container flex">
  <div class="col-6">
       <?php get_template_part( 'template-parts/content', 'featured' );
       
          if(comments_open() || get_comments_number() ) {
            comments_template();
        }
        
      
    ?>
   </div>
   <div class="sidebarsingle col-6 ">
   <?php get_sidebar('contato');?>
   </div>
</div>


<?php get_footer(); ?>