<article <?php post_class();?>>           
              <h2><?php the_title(); ?></h2>
              <?php the_post_thumbnail('medium'); ?>
              <p>published in <?php echo get_the_date(); ?> by <?php the_author_posts_link(); ?></p>
              <p><?php the_tags('Tags: ', ', ');?></p>
              <?php the_excerpt(); ?>
              <hr>

        </article>