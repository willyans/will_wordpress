<article <?php post_class();?>>           
<a class="apost" href="<?php the_permalink();?>"><h2><?php the_title(); ?></h2></a>
              <?php the_post_thumbnail('medium'); ?>
              <p>published in <?php echo get_the_date(); ?> by <?php the_author_posts_link(); ?></p>
              <p>categories: <?php the_category(' '); ?></p>
              <p><?php the_tags('Tags: ', ', ');?></p>
              <?php the_content(); ?>
              <hr>

        </article>